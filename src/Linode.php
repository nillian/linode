<?php namespace Hampel\Linode;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ParseException;
use GuzzleHttp\Exception\RequestException;
use Hampel\Linode\Commands\CommandInterface;
use Hampel\Linode\Exception\LinodeDataException;
use Hampel\Linode\Exception\LinodeErrorException;
use Hampel\Linode\Exception\LinodeParseException;
use Hampel\Linode\Exception\LinodeRequestException;
use Hampel\Linode\Exception\LinodeResponseException;

/**
 * The main service interface using Guzzle
 *
 */
class Linode
{
	/** @var string base url for API calls */
	protected static $base_url = 'https://api.linode.com/';

	/** @var string api_key */
	protected $api_key;

	/** @var Client our Guzzle HTTP Client object */
	protected $client;

	/** @var Response Guzzle Response object representing the last response from Guzzle call to Linode API */
	protected $last_response;

	protected $last_action;

	/**
	 * Constructor
	 *
	 * @param Client $client	Guzzle HTTP client
	 * @param string $api_key	Linode api key
	 */
	public function __construct(Client $client, $api_key = "")
	{
		$this->client = $client;
		$this->api_key = $api_key;
	}

	/**
	 * Make - construct a service object
	 *
	 * @param string $api_key
	 * @return LinodeService a fully hydrated Linode Service, ready to run
	 */
	public static function make($api_key)
	{
		$config = ['base_url' => self::$base_url];

		$client = new Client($config);

		return new static($client, $api_key);
	}

	/**
	 * Make a call to the API via Guzzle
	 *
	 * @param CommandInterface $command		Command to send
	 *
	 * @return mixed						Data returned from api call
	 */
	public function execute(CommandInterface $command)
	{
		$action = $command->getAction();
		$this->last_action = $action;

		return $this->send($command->build($this->api_key));
	}

	protected function send($options)
	{
		$guzzle_options = ['query' => $options];

		try
		{
			$response = $this->client->get('', $guzzle_options);
		}
		catch (RequestException $e)
		{
			throw new LinodeRequestException("Error requesting Linode command [{$this->last_action}]: " . $e->getMessage(), $e->getCode(), $e);
		}

		$status_code = $response->getStatusCode();

		if ($status_code != 200) throw new LinodeResponseException("Linode returned status code [{$status_code}] from command [{$this->last_action}]: " . $response->getReasonPhrase(), $status_code, null, $response);

		$this->last_response = $response;

		return $this->processResponse($response);
	}

	protected function processResponse($response)
	{
		try
		{
			return $this->parseResponse($response->json());
		}
		catch (ParseException $e)
		{
			throw new LinodeParseException("Error decoding JSON data returned from Linode command [{$this->last_action}]: " . $e->getMessage(), $e->getCode(), $e, $response);
		}
	}

	protected function parseResponse($data)
	{
		if (empty($data)) throw new LinodeDataException("Empty body received from command [{$this->last_action}]", 0, null, $this->last_response);

		if (!array_key_exists('ERRORARRAY', $data)) throw new LinodeDataException("Invalid data received from command [{$this->last_action}] - no ERRORARRAY", 0, null, $this->last_response);
		if (!array_key_exists('DATA', $data)) throw new LinodeDataException("Invalid data received from command [{$this->last_action}] - no DATA", 0, null, $this->last_response);
		if (!array_key_exists('ACTION', $data)) throw new LinodeDataException("Invalid data received from command [{$this->last_action}] - no ACTION", 0, null, $this->last_response);

		if (!empty($data['ERRORARRAY'])) throw new LinodeErrorException($data['ERRORARRAY'], $data['ACTION'], $this->last_response);

		return $data['DATA'];
	}

	/**
	 * Return the response object from the last API call made
	 *
	 * @return Response Guzzle Reponse object
	 */
	public function getLastResponse()
	{
		return $this->last_response;
	}

	/**
	 * Return the status code from the last API call made
	 *
	 * @return number status code
	 */
	public function getLastStatusCode()
	{
		$last_response = $this->getLastResponse();
		if (! is_null($last_response))
		{
			return $last_response->getStatusCode();
		}
	}

	public function getLastQuery()
	{
		$last_response = $this->getLastResponse();
		if (! is_null($last_response))
		{
			return $last_response->getEffectiveUrl();
		}
	}

	public function setApiKey($api_key)
	{
		$this->api_key = $api_key;
	}
}
