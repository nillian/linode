<?php namespace Hampel\Linode\Commands;

class LinodeJobCommand extends Command
{
	/** @var string the command prefix */
	protected $prefix = 'linode.job';

    /** @var array allowable actions for $action parameter */
	protected $allowed_actions = ['list'];
	
	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [
		'linodeid', // required - numeric LinodeId
		'jobid', // optional - numeric Limits the list to the specified JobID
		'pendingonly',  // optional - boolean 
	];
}
