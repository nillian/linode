<?php namespace Hampel\Linode\Commands;

class LinodeIpCommand extends Command
{
	/** @var string the command prefix */
	protected $prefix = 'linode.ip';

    /** @var array allowable actions for $action parameter */
	protected $allowed_actions = ['addprivate','addpublic','list','setrdns','swap'];
	
	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [
		'linodeid', // required - numeric LinodeId
		'ipAddressid', // optional - numeric If specified, limits the result to this IPAddressID
	];
}
