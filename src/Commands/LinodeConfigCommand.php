<?php namespace Hampel\Linode\Commands;

class LinodeConfigCommand extends Command
{
	/** @var string the command prefix */
	protected $prefix = 'linode.config';

    /** @var array allowable actions for $action parameter */
	protected $allowed_actions = ['create', 'delete', 'list', 'update'];
	
	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [
		'linodeid', // required - numeric LinodeID
		'kernelid', // required - numeric The KernelID for this profile. Found in avail.kernels()
		'label',  // required - string the label for this profile
		'comments', // optional - string Comments you wish to save along with this profile
		'ramlimit', // optional - numeric RAMLimit in MB. 0 for max. 
		'disklist', // optional - string A comma delimited list of DiskIDs; position reflects device node. The 9th element for specifying the initrd.
		'runlevel', // optional - One of 'default', 'single', 'binbash'
		'rootdevicenum', // optional - numeric Which device number (1-8) that contains the root partition. 0 to utilize RootDeviceCustom.
		'rootdevicecustom', // optional - string A custom root device setting.
		'rootdevicero', // optional - boolean Enables the 'ro' kernel flag. Modern distros want this.
		'helper_disableupdatedb', // optional - boolean Enable the disableUpdateDB filesystem helper
		'helper_xen', // optional - boolean
		'helper_depmod', // optional - boolean Creates an empty modprobe file for the kernel you're booting.
	    'devtmpfs_automount', // optional - boolean Controls if pv_ops kernels should automount devtmpfs at boot.	
	];
}
