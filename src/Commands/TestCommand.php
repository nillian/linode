<?php namespace Hampel\Linode\Commands;

class TestCommand extends Command
{
	/** @var string the command prefix */
	protected $prefix = 'test';

	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [];

	/** @var array allowable actions for $action parameter */
	protected $allowed_actions = ['echo'];

}
