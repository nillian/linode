<?php namespace Hampel\Linode\Exception;

use GuzzleHttp\Message\Response;

/**
 * Custom Linode Exception which provides additional information for Linode errors
 *
 */
class LinodeErrorException extends LinodeException
{
	/** @var array extra information about Linode errors. Not currently used! */
	protected $linode_errors = array(
		0 => "OK",
		1 => "Bad request",
		2 => "No action was requested",
		3 => "The requested class does not exist",
		4 => "Authentication failed",
		5 => "Object not found",
		6 => "A required property is missing for this action",
		7 => "Property is invalid",
		8 => "A data validation error has occurred",
		9 => "Method not implemented",
		10 => "Too many batched requests",
		11 => "RequestArray isn't valid JSON or WDDX",
		12 => "Batch approaching timeout. Stopping here.",
		13 => "Permission denied",
		14 => "API rate limit exceeded",
		30 => "Charging the credit card failed",
		31 => "Credit card is expired",
		40 => "Limit of Linodes added per hour reached",
		41 => "Linode must have no disks before delete"
	);

	protected function process_error_array(array $errors)
	{
		$error_string = "";
		$error_strings = array();

		if (!empty($errors) AND is_array($errors))
		{
			foreach ($errors as $error)
			{
				$code = array_key_exists('ERRORCODE', $error) ? $error['ERRORCODE'] : "";
				$message = array_key_exists('ERRORMESSAGE', $error) ? $error['ERRORMESSAGE'] : "";

				$error_strings[] = "[{$code}] {$message}";
			}

			$error_string = implode("; ", $error_strings);
		}

		return $error_string;
	}

	public function __construct($errors, $command, Response $response)
	{
		$code = isset($errors[0]['ERRORCODE']) ? $errors[0]['ERRORCODE'] : 0; // set our error code to the first of the codes returned by Linode
		$message = "Error processing Linode command [{$command}]: " . $this->process_error_array($errors);

		parent::__construct($message, $code, null, $response);
	}
}
