<?php namespace Hampel\Linode\Commands;

use Mockery;
use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class LinodeCommandTest extends \PHPUnit_Framework_TestCase
{
    protected $linode;

    public function setUp()
    {
        date_default_timezone_set('UTC');
        $this->linode = Linode::make(API_KEY);

        $this->mock = new Mock();

        $this->client = new Client();
        $this->client->getEmitter()->attach($this->mock);
    }

    /**
     * Where we store sample JSON responses.
     * @return string
     */
    protected function getMockPath()
    {
        return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
    }

    /**
     * @group network
     */
    public function testList()
    {
        $response = $this->linode->execute(new LinodeCommand('list', []));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
        $this->assertTrue(is_array($response)); // you may not have any linodes...
    }

    /**
     * @group network
     * @expectedException \Hampel\Linode\Exception\LinodeErrorException
     * @expectedExceptionMessage Error processing Linode command
     */
    public function testFailCreate()
    {
        // Omit missing parameters to trigger an error
        $response = $this->linode->execute(new LinodeCommand('create', []));
    }

    /**
     * Mock...
     */
    public function testMockCreate()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.create');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.create',
            'datacenterid' => 2,
            'planid' => 2,
            'paymentterm' => 1]);

        $this->mock->addResponse($this->getMockPath() . 'linode_create.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.create&datacenterid=2&planid=2&paymentterm=1', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('LinodeID', $response);
        $this->assertEquals('8098', $response['LinodeID']);

    }

    /**
     *
     */
    public function testMockBoot()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.boot');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.boot',
            'linodeid' => 123,
            'configid' => 123]);

        $this->mock->addResponse($this->getMockPath() . 'linode_boot.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.boot&linodeid=123&configid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('1293', $response['JobID']);
    }

    /**
     *
     */
    public function testMockClone()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.clone');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.clone',
            'linodeid' => 123,
            'datacenterid' => 123,
            'planid' => 123]);

        $this->mock->addResponse($this->getMockPath() . 'linode_clone.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.clone&linodeid=123&datacenterid=123&planid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('LinodeID', $response);
        $this->assertEquals('8098', $response['LinodeID']);
    }

    /**
     *
     */
    public function testMockDelete()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.delete');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.delete',
            'linodeid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_delete.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.delete&linodeid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('LinodeID', $response);
        $this->assertEquals('8204', $response['LinodeID']);
    }

    /**
     *
     */
    public function testMockList()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.list');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.list'
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_list.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.list', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertTrue(is_array($response));
    }


    /**
     *
     */
    public function testMockReboot()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.reboot');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.reboot',
            'linodeid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_reboot.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.reboot&linodeid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('1295', $response['JobID']);
    }

    /**
     *
     */
    public function testMockResize()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.resize');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.resize',
            'linodeid' => 123,
            'configid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_resize.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.resize&linodeid=123&configid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        //$this->assertArrayHasKey('JobID', $response);
        //$this->assertEquals('1295', $response['JobID']);
    }

    /**
     *
     */
    public function testMockShutdown()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.shutdown');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.shutdown',
            'linodeid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_shutdown.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.shutdown&linodeid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('1292', $response['JobID']);
    }

    /**
     *
     */
    public function testMockUpdate()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.update');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.update',
            'linodeid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_update.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.update&linodeid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('LinodeID', $response);
        $this->assertEquals('8098', $response['LinodeID']);
    }

    /**
     *
     */
    public function tearDown()
    {
        Mockery::close();
    }
}
