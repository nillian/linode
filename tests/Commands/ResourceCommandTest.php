<?php namespace Hampel\Linode\Commands;

use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class ResourceCommandTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->mock = new Mock();

		$this->client = new Client();
		$this->client->getEmitter()->attach($this->mock);

		$this->linode = new Linode($this->client);
	}

	protected function getMockPath()
	{
		return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
	}

	public function testCreate()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_resource_create.json');

		$response = $this->linode->execute(new ResourceCommand('create',
			[
				'domainid' => 12345,
				'type' => 'A',
				'target' => '10.0.0.1'
			]
		));

		$this->assertEquals('?api_action=domain.resource.create&domainid=12345&type=A&target=10.0.0.1', $this->linode->getLastQuery());
		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey('ResourceID', $response);
		$this->assertEquals(54321, $response['ResourceID']);
	}

	public function testUpdate()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_resource_update.json');

		$response = $this->linode->execute(new ResourceCommand('update',
			[
				'domainid' => 12345,
				'resourceid' => 54321,
				'target' => '10.0.0.1'
			]
		));

		$this->assertEquals('?api_action=domain.resource.update&domainid=12345&resourceid=54321&target=10.0.0.1', $this->linode->getLastQuery());
		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey('ResourceID', $response);
		$this->assertEquals(54321, $response['ResourceID']);
	}

	public function testDelete()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_resource_delete.json');

		//$record = new Record($this->linode);
		//$response = $record->delete(12345, 54321);
		$response = $this->linode->execute(new ResourceCommand('delete',
			[
				'domainid' => 12345,
				'resourceid' => 54321
			]
		));

		$this->assertEquals('?api_action=domain.resource.delete&domainid=12345&resourceid=54321', $this->linode->getLastQuery());
		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey('ResourceID', $response);
		$this->assertEquals(54321, $response['ResourceID']);
	}

	public function testListSingle()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_resource_list_single.json');

		$response = $this->linode->execute(new ResourceCommand('list',
			[
				'domainid' => 12345,
				'resourceid' => 54321
			]
		));

		$this->assertEquals('?api_action=domain.resource.list&domainid=12345&resourceid=54321', $this->linode->getLastQuery());
		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey(0, $response);
		$this->assertTrue(is_array($response[0]));
		$this->assertArrayHasKey('RESOURCEID', $response[0]);
		$this->assertEquals(54321, $response[0]['RESOURCEID']);
		$this->assertArrayHasKey('TYPE', $response[0]);
		$this->assertEquals('A', $response[0]['TYPE']);
	}

	public function testList()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_resource_list_multiple.json');

		$response = $this->linode->execute(new ResourceCommand('list', ['domainid' => 12345]));

		$this->assertEquals('?api_action=domain.resource.list&domainid=12345', $this->linode->getLastQuery());
		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertEquals(2, count($response));
		$this->assertArrayHasKey(0, $response);
		$this->assertTrue(is_array($response[0]));
		$this->assertArrayHasKey('RESOURCEID', $response[0]);
		$this->assertEquals(54321, $response[0]['RESOURCEID']);
		$this->assertArrayHasKey('TYPE', $response[0]);
		$this->assertEquals('A', $response[0]['TYPE']);
		$this->assertArrayHasKey(1, $response);
		$this->assertArrayHasKey('RESOURCEID', $response[1]);
		$this->assertEquals(54322, $response[1]['RESOURCEID']);
		$this->assertArrayHasKey('TYPE', $response[1]);
		$this->assertEquals('A', $response[1]['TYPE']);
	}
}
