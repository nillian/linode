<?php namespace Hampel\Linode\Commands;

use Mockery;
use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class LinodeDiskCommandTest extends \PHPUnit_Framework_TestCase
{

    protected $linode;
    protected $client;
    protected $command;

    public function setUp()
    {
        date_default_timezone_set('UTC');

        $this->mock = new Mock();

        $this->client = new Client();
        $this->client->getEmitter()->attach($this->mock);
    }

    /**
     * Where we store sample JSON responses.
     * @return string
     */
    protected function getMockPath()
    {
        return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
    }

    /**
     * Mock...
     */
    public function testMockCreate()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.disk.create');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.disk.create',
            'linodeid' => 123,
            'label' => 'Test',
            'type' => 'ext3'
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_disk_create.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.disk.create&linodeid=123&label=Test&type=ext3', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('1298', $response['JobID']);

    }

    /**
     * Mock...
     */
    public function testMockCreateFromDistribution()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.disk.createfromdistribution');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.disk.createfromdistribution',
            'linodeid' => 123,
            'distributionid' => 123,
            'label' => 'Test',
            'size' => 2048,
            'rootpass' => 'xxxxxxxx'
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_disk_createfromdistribution.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.disk.createfromdistribution&linodeid=123&distributionid=123&label=Test&size=2048&rootpass=xxxxxxxx', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('1298', $response['JobID']);

    }

    /**
     * Mock...
     */
    public function testMockCreateFromImage()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.disk.createfromimage');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.disk.createfromimage',
            'imageid' => 123,
            'linodeid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_disk_createfromimage.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.disk.createfromimage&imageid=123&linodeid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('999', $response['JobID']);

    }

    /**
     * Mock...
     */
    public function testMockCreateFromStackScript()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.disk.createfromstackscript');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.disk.createfromstackscript',
            'linodeid' => 123,
            'stackscriptid' => 123,
            'stackscriptudfresponses'=>'{}',
            'distributionid' => 123,
            'label' => 'Test',
            'size' => 2048,
            'rootpass' => 'xxxxxxxx'
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_disk_createfromstackscript.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.disk.createfromstackscript&linodeid=123&stackscriptid=123&stackscriptudfresponses=%7B%7D&distributionid=123&label=Test&size=2048&rootpass=xxxxxxxx', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('445', $response['JobID']);

    }

    /**
     *
     */
    public function testMockDelete()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.disk.delete');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.disk.delete',
            'linodeid' => 123,
            'diskid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_disk_delete.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.disk.delete&linodeid=123&diskid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('1298', $response['JobID']);

    }

    /**
     *
     */
    public function testMockDuplicate()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.disk.duplicate');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.disk.duplicate',
            'linodeid' => 123,
            'diskid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_disk_duplicate.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.disk.duplicate&linodeid=123&diskid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('1299', $response['JobID']);

    }

    /**
     *
     */
    public function testMockImagize()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.disk.imagize');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.disk.imagize',
            'linodeid' => 123,
            'diskid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_disk_imagize.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.disk.imagize&linodeid=123&diskid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('5693', $response['JobID']);

    }

    /**
     *
     */
    public function testMockList()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.disk.list');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.disk.list',
            'linodeid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_disk_list.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.disk.list&linodeid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertTrue(is_array($response));
    }

    /**
     *
     */
    public function testMockResize()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.disk.resize');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.disk.resize',
            'linodeid' => 123,
            'diskid' => 123,
            'size' => 2048
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_disk_resize.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.disk.resize&linodeid=123&diskid=123&size=2048', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('JobID', $response);
        $this->assertEquals('1298', $response['JobID']);
    }

    /**
     *
     */
    public function testMockUpdate()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.disk.update');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.disk.update',
            'linodeid' => 123,
            'diskid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_disk_update.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.disk.update&linodeid=123&diskid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('DiskID', $response);
        $this->assertEquals('55647', $response['DiskID']);
    }

    /**
     *
     */
    public function tearDown()
    {
        Mockery::close();
    }
}
