<?php namespace Hampel\Linode\Commands;

use Mockery;
use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class NodebalancerConfigCommandTest extends \PHPUnit_Framework_TestCase
{
    protected $linode;

    public function setUp()
    {
        date_default_timezone_set('UTC');
        $this->linode = Linode::make(API_KEY);

        $this->mock = new Mock();

        $this->client = new Client();
        $this->client->getEmitter()->attach($this->mock);
    }

    /**
     * Where we store sample JSON responses.
     * @return string
     */
    protected function getMockPath()
    {
        return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
    }

    /**
     * This will actually create a nodebalancer, check that it shows up in the list, and then
     * delete it.  Running this test will cost you money!  Everything is put into one test
     * so we can more easily clean up.
     * TODO: write the network tests to test this for real
     * @group chargeable
     */
//    public function testCreateListUpdateAndDelete()
//    {
//        // create
//        $response = $this->linode->execute(new NodebalancerCommand('create', [
//            'datacenterid' => 2,
//            'paymentterm' => 1, // required, but NOT DOCUMENTED!!!
//            'label' => 'Test' // ignored
//        ]));
//        $this->assertEquals(200, $this->linode->getLastStatusCode());
//        $this->assertArrayHasKey('NodeBalancerID', $response);
//
//        $nodebalancerid = $response['NodeBalancerID'];
//
//        echo "Created Nodebalancer {$nodebalancerid} (your credit card may have been charged)\n";
//
//        // list
//        $response = $this->linode->execute(new NodebalancerCommand('list', [
//            'nodebalancerid' => $nodebalancerid
//        ]));
//        $this->assertEquals(200, $this->linode->getLastStatusCode());
//        $this->assertTrue(is_array($response));
//        $this->assertTrue(count($response) == 1);
//        //$n = array_shift($response);
//        //$this->assertArrayHasKey('Label', $n);
//        //$this->assertEquals('Testing', $n['Label']);
//
//        // update
//        $response = $this->linode->execute(new NodebalancerCommand('update', [
//            'nodebalancerid' => $nodebalancerid,
//            'label' => 'Testing'
//        ]));
//        $this->assertEquals(200, $this->linode->getLastStatusCode());
//        $this->assertArrayHasKey('NodeBalancerID', $response);
//        echo "Updated Nodebalancer {$nodebalancerid}\n";
//
//        // delete
//        $response = $this->linode->execute(new NodebalancerCommand('delete', [
//            'nodebalancerid' => $nodebalancerid
//        ]));
//        $this->assertEquals(200, $this->linode->getLastStatusCode());
//        $this->assertArrayHasKey('NodeBalancerID', $response);
//        echo "Deleted Nodebalancer {$nodebalancerid} (your credit card should have been refunded)\n";
//
//    }

    /**
     * @group network
     */
    public function testExceptionCreate()
    {
        $this->setExpectedException('Hampel\Linode\Exception\LinodeErrorException', 'Error processing Linode command [nodebalancer.config.create]');

        $response = $this->linode->execute(new NodebalancerConfigCommand('create', []));
    }


    /**
     *
     */
    public function testMockCreate()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('nodebalancer.config.create');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'nodebalancer.config.create',
            'nodebalancerid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'nodebalancer_config_create.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=nodebalancer.config.create&nodebalancerid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('ConfigID', $response);
        $this->assertEquals(38, $response['ConfigID']);

    }

    /**
     *
     */
    public function testMockDelete()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('nodebalancer.config.delete');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'nodebalancer.config.delete',
            'nodebalancerid' => 123,
            'configid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'nodebalancer_config_delete.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=nodebalancer.config.delete&nodebalancerid=123&configid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('ConfigID', $response);
        $this->assertEquals(38, $response['ConfigID']);
    }

    /**
     *
     */
    public function testMockList()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('nodebalancer.config.list');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'nodebalancer.config.list'
        ]);

        $this->mock->addResponse($this->getMockPath() . 'nodebalancer_config_list.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=nodebalancer.config.list', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertTrue(is_array($response));
    }

    /**
     *
     */
    public function testMockUpdate()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('nodebalancer.config.update');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'nodebalancer.config.update',
            'configid' => 38,
            'port' => 222,
        ]);

        $this->mock->addResponse($this->getMockPath() . 'nodebalancer_config_update.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=nodebalancer.config.update&configid=38&port=222', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('ConfigID', $response);
        $this->assertEquals(38, $response['ConfigID']);
    }


    /**
     *
     */
    public function tearDown()
    {
        Mockery::close();
    }

}
