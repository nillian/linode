<?php namespace Hampel\Linode\Commands;

use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class UserCommandTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->mock = new Mock();

		$this->client = new Client();
		$this->client->getEmitter()->attach($this->mock);

		$this->linode = new Linode($this->client);
	}

	protected function getMockPath()
	{
		return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
	}

	public function testGetAPIKey()
	{
		$this->mock->addResponse($this->getMockPath() . 'user_getapikey.json');

		$response = $this->linode->execute(new UserCommand('getapikey', ['username' => 'foo', 'password' => 'bar']));

		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey('USERNAME', $response);
		$this->assertEquals("mock_username", $response['USERNAME']);
		$this->assertArrayHasKey('API_KEY', $response);
		$this->assertEquals("mock_api_key", $response['API_KEY']);
	}
}
